@auth
@extends('layouts.layout')

@section('description')
<div>
    <h4>Update Profile</h4>
    <p>Please update your personal profile regularly to avoid any miscommunication</p>
</div>
@endsection

@section('content')
@if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
@endif
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br/>
@endif
<div class="container-fluid personal">
    <form method="post" action="{{ route('profile.update', $user->id) }}"  enctype="multipart/form-data">
        @method('PATCH')
        @csrf
        <div class="row">
            <div class="col-md-12 profile-edit">
                <div class="row">
                    <div class="col-md-3">
                        <div id="circle" style=""> </div>
                    </div>
                    <div class="col-md-8 offset-md-1">
                        <p class="profile-name">Tukar Gambar Profile</p>
                        <div class="form-group">
                            <input id="files" type="file" class="form-control textbox-style" name="pictures[]"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <br>
        <br>
        <div class="form-style">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="name" class="gray-hint">Nama</label>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <input type="text" class="form-control textbox-style" name="name"  value="{{$user->name}}" />
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="age" class="gray-hint">Umur</label>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <input type="number" class="form-control textbox-style" name="age"  value="{{$user->age}}" />
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="phone_number" class="gray-hint">No. Phone</label>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <input type="text" class="form-control textbox-style" name="phone_number"  value="{{$user->phone_number}}" />
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            @if ($user->role == 'agent')
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="leader_id" class="gray-hint">Leader</label>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <select class="form-control textbox-style" name="leader_id"   @if(Auth::user()->role != 'admin') disabled }} @endif>
                            @foreach($users as $current_user)
                                <option value="{{$current_user->id}}" @if($user->leader_id == $current_user->id) selected @endif >{{$current_user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="address" class="gray-hint">Alamat Rumah</label>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                            <textarea class="form-control" rows="3" cols="60" name="address">{{$user->address }}</textarea>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="description" class="gray-hint">Kenapa anda berminat menjadi agent The Famouse Jerux?</label>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <textarea class="form-control" rows="5" cols="60" name="description">{{ $user->description }} </textarea>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bank_name" class="gray-hint">Nama Bank</label>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <input type="text" class="form-control textbox-style" name="bank_name"  value="{{$user->bank_name}}" />
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bank_number" class="gray-hint">Bank Account</label>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                        <input type="text" class="form-control textbox-style" name="bank_number"  value="{{$user->bank_number}}" />
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3 offset-md-9">
                    <div class="form-group button-holder">
                        <button type="submit" class="btn btn-primary yellow-btn">SAVE & UPDATE</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('styles')
    @if(!empty($user->picture[0]->file_name))
    <style>
            #circle
            {
                background:url({{Storage::url($user->picture[0]->file_name)}});
                background-size: cover;
                border-radius:50% 50% 50% 50%;
                width:200px;
                height:200px;
                margin: 0 auto;
            }
    </style>
    @else
    <style>
        #circle
        {
            background:url({{Storage::url('blank.png')}});
            background-size: cover;
            border-radius:50% 50% 50% 50%;
            width:200px;
            height:200px;
            margin: 0 auto;
        }
    </style>
    @endif
@endsection
@else
    <script>window.location = "/";</script>
@endauth
