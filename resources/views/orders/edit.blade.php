@extends('orders.header')

@section('ordercontent')

<form method="post" action="{{ route('orders.update', $order->id) }}"  enctype="multipart/form-data">
    @method('PATCH')
    @csrf
    <div class="form-style">
        <div class="row">
            <div class="col-md-12 white-background purchase">
                <div class="row">
                    <div class="col-md-12 header-purchase">
                        <h5> Edit Purchase</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 no-right-padding">
                        <div class="black-background white">
                            <p class="no-bottom-margin">Customer Details</p>
                        </div>
                        <div class="gray-background ">
                            <button type="button" onclick="window.location.href = '{{ route('customerorders.index') }}';" class="btn gray-background">Add a new customer <i class="fas fa-plus"></i></button>
                        </div>
                        <div class="gray-background ">
                            <select class="form-control" id='customers'name="customer_id">
                                <option  disabled selected>Select Existing Customer</option>
                                @foreach($user->customers as $customer)
                                    <option value="{{$customer->id}}"
                                    @if ($customer->id == $order->customer_id)
                                    selected
                                    @endif >{{$customer->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="gray-background customer-info " id="userdata">
                        </div>
                    </div>
                    <div class="col-md-5 no-side-padding">
                        <div class="black-background white">
                            <p class="no-bottom-margin">Order Details</p>
                        </div>
                        <div class="gray-background">
                            <div class="row">
                                <div class="col-md-4 ">
                                    <p>Product</p>
                                </div>
                                <div class="col-md-4">
                                    <p>Quantity</p>
                                </div>
                                <div class="col-md-4">
                                    <p>Price</p>
                                </div>
                            </div>

                            @foreach($order->orderproducts as $orderproduct)
                            <div class="row">
                                <div class="col-md-4 ">
                                    <p>{{$orderproduct->product->name}}</p>
                                    <input type="hidden" class="form-control" name="product_id[]" value={{$orderproduct->product->id}} />
                                    <input type="hidden" class="form-control" name="orderproduct_id[]" value={{$orderproduct->id}} />
                                    <input type="hidden" class="form-control" name="price[]" value={{$orderproduct->product->price}} />
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="number" class="form-control" name="quantity[]" value={{$orderproduct->quantity}} />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <p>RM {{$orderproduct->product->price}}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-2 no-left-padding">
                        <div class="black-background white">
                            <p class="no-bottom-margin">Action</p>
                        </div>
                        <div class="gray-background">
                            <button type="submit" class="btn">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection



