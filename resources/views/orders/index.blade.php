
@extends('layouts.layout')

@section('description')
<div>
    <h4>Approve Orders</h4>
    <p>Pending Order for Stokis Approval are listed here.</p>
</div>
@endsection

@section('content')
@if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
@endif
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br/>
@endif
<div class="container-fluid dashboard">
    <div class="row">
        <div class="col-md-12 white-background purchase">
            <div class="row">
                <div class="col-md-8 header-purchase">
                    @if (Route::currentRouteName() == 'orders.approve')
                    <h5>  Pending Order for Stokis Approval </h5>
                    @else
                    <h5>  Order History</h5>
                    @endif
                </div>
                <div class="col-md-4 header-purchase text-right">
                    @if (Route::currentRouteName() == 'orders.approve')
                    <form method="get" action="{{ route('orders.stokisapprove', Auth::user()->id) }}"  enctype="multipart/form-data">
                        @csrf
                        <button type="submit" class="btn gray-background ">Submit Order to HQ</button>
                    </form>
                    @endif
                </div>
            </div>

            <table class="table-bordered order-table">
                <thead>
                    <tr>
                        <th>Order Number</th>
                        <th>Customer ID</th>
                        <th>Agent ID</th>
                        <th>Agent Details</th>
                        <th>Order Details</th>
                        <th>Total Price</th>
                        <th>Last Updated</th>
                        @if (Route::currentRouteName() == 'orders.approve')
                        <th>Action</th>
                        @else
                        <th>Status</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                <tr>
                    <td>JON{{date('dmy', strtotime($order->created_at))}}-{{str_pad($order->id, 4, '0', STR_PAD_LEFT)}}</td>
                    <td>JCI{{date('dmy', strtotime($order->customer->created_at))}}-{{str_pad($order->customer->id, 4, '0', STR_PAD_LEFT)}}</td>
                    <td>JSI{{date('dmy', strtotime($order->user->created_at))}}-{{str_pad($order->user->id, 4, '0', STR_PAD_LEFT)}}</td>
                    <td>
                        <p>Name : {{$order->user->name}}</p>
                        <p>Phone : {{$order->user->phone_number}}</p>
                        <p>Address : {{$order->user->address}}</p>
                    </td>
                    <td style="  white-space: nowrap;">
                    @foreach($order->orderproducts as $orderproduct)
                        <p>{{$orderproduct->product->name}} x {{$orderproduct->quantity}} unit</p>
                    @endforeach
                    </td>
                    <td>RM {{$order->total}}</td>
                    <td>{{$order->updated_at}}</td>
                    @if (Route::currentRouteName() == 'orders.approve')
                    <td>
                        <form method="get" action="{{ route('orders.stokisreject', $order->id) }}"  enctype="multipart/form-data">
                            @csrf
                            <button type="submit" class="btn gray-background ">Reject</button>
                        </form>
                    </td>
                    @else
                    <td>{{$order->order_status}} </td>
                    @endif
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="text-xs-center">
        {{ $orders->links() }}
    </div>
</div>
@endsection
