@auth
@extends('layouts.layout')

@section('description')
<div>
    <h4>Dashboard Overview</h4>
    <p> Welcome to Jerux Liur Leleh Product Purchase Dashboard.</p>
</div>
@endsection

@section('content')
@if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
@endif
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br/>
@endif
<div class="container-fluid dashboard">
    <div class="row">
        <div class="col-md-12 white-background profile small-font">
            <div class="row">
                <div class="col-md-4">
                    <div id="circle" style=""> </div>
                </div>
                <div class="col-md-8">
                   <p class="profile-name">{{$user->name}}</p>
                   <br>
                   <hr>
                   <p>Umur : {{$user->age}}</p>
                   <p>Location : {{$user->address}}</p>
                   <p>Register Date : {{date('d-m-Y', strtotime($user->created_at))}}</p>
                </div>
            </div>
        </div>
    </div>
    @yield('ordercontent')
</div>
@endsection



@section('styles')
    @if(!empty($user->picture[0]->file_name))
    <style>
            #circle
            {
                background:url({{Storage::url($user->picture[0]->file_name)}});
                background-size: cover;
                border-radius:50% 50% 50% 50%;
                width:150px;
                height:150px;
                margin: 0 auto;
            }
    </style>
    @else
    <style>
        #circle
        {
            background:url({{Storage::url('blank.png')}});
            background-size: cover;
            border-radius:50% 50% 50% 50%;
            width:150px;
            height:150px;
            margin: 0 auto;
        }
    </style>
    @endif
@endsection

@section('scripts')
<script>
    $(function() {
            $('select[name=customer_id]').change(function() {
                console.log('lul');
                var url = '{{ url('customer') }}' + '/get/'+ $(this).val();

                $.get(url, function(data) {
                    var id = ('0000'+data.id).slice(-4);
                    var textbox = $('#userdata');
                    textbox.empty();
                    let mydate = new Date(data.created_at);
                    let day= ('00'+mydate.getDate()).slice(-2);
                    let month= mydate.getMonth()+1
                    month =  ('00'+month).slice(-2);
                    let year=mydate.getFullYear().toString().substr(-2);
                    let formatted_date =day+ month+year;
                    textbox.append('<p> Customer ID : JCI' + formatted_date+ '-'+ id + '</p>');
                    textbox.append('<p> Name: ' + data.name + '</p>');
                    textbox.append('<p> Phone: ' +  data.phone_number +'</p>');
                    textbox.append('<p> Address: ' + data.address + '</p>');

                });
            });
        });
</script>
<script>
function updatePrice(){
    var price = document.getElementsByName('price[]');
    var quantity = document.getElementsByName('quantity[]');
    var priceInput = document.getElementById('totalPrice');
    var totalPrice = 0;

    for (var i = 0; i <price.length; i++) {
        var linePrice = price[i].value * quantity[i].value;
        totalPrice = totalPrice + linePrice;
    }
    totalPrice.toFixed(totalPrice);
    priceInput.innerHTML = 'RM ' + totalPrice + '.00';
}
</script>
@endsection
@else
    <script>window.location = "/";</script>
@endauth
