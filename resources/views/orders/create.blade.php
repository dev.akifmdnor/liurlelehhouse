@extends('orders.header')

@section('ordercontent')

@if(Auth::user()->role == 'stokis' || Auth::user()->role == 'agent' )

<form method="post" action="{{ route('orders.store') }}"  enctype="multipart/form-data">
    @csrf
    <div class="form-style">
        <div class="row">
            <div class="col-md-12 white-background purchase">
                <div class="row">
                    <div class="col-md-12 header-purchase">
                        <h5> Add New Purchase</h5>
                    </div>
                </div>
                <table class="table-bordered order-table">
                    <thead>
                        <tr>
                            <th>Customer Details</th>
                            <th>Order Details</th>
                            <th>Total Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="gray-background">
                                    <button type="button" class="btn gray-background my-1 mx-2"onclick="window.location.href = '{{ route('customerorders.index') }}';" class="btn gray-background">Add a new customer <i class="fas fa-plus"></i></button>
                                </div>
                                <div class="gray-background">
                                    <select class="form-control" id='customers'name="customer_id">
                                        <option  disabled selected>Select Existing Customer</option>
                                        @foreach($user->customers as $customer)
                                            <option value="{{$customer->id}}">{{$customer->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="gray-background customer-info " id="userdata">
                                </div>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><b>Product</b></p>
                                    </div>
                                    <div class="col-md-4">
                                        <p><b>Quantity</b></p>
                                    </div>
                                    <div class="col-md-4">
                                        <p><b>Price</b></p>
                                    </div>
                                </div>
                                @foreach($products as $product)
                                <div class="row">
                                    <div class="col-md-4">
                                        <p>{{$product->name}}</p>
                                        <input type="hidden" class="form-control" name="product_id[]" value={{$product->id}} />
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="number" class="form-control" min="0" name="quantity[]" value="0" onChange="updatePrice()"/>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p>RM {{$product->price}}</p>
                                        <input type="hidden" class="form-control" name="price[]" value={{$product->price}} />
                                    </div>
                                </div>
                                @endforeach
                            </td>
                            <td>
                                <div class="form-group">
                                    <p id="totalPrice"> RM 0.00</p>
                                </div>
                            </td>
                            <td>
                                <button type="submit" class=" gray-background">Submit</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>
@endif
<div class="row">
    <div class="col-md-12 white-background purchase">
        <div class="row">
            <div class="col-md-12 header-purchase">
                <h5>  Product Purchase Status</h5>
            </div>
        </div>
        <table class="table-bordered order-table">
            <thead>
                <tr>
                    <th>Order Number</th>
                    <th>Customer ID</th>
                    <th>Customer Details</th>
                    <th>Order Details</th>
                    <th>Total Price</th>
                    <th>Order Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
            <form method="get" action="{{ route('orders.edit', $order->id) }}"  enctype="multipart/form-data">
                @csrf
                <tr>
                    <td>JON{{date('dmy', strtotime($order->created_at))}}-{{str_pad($order->id, 4, '0', STR_PAD_LEFT)}}</td>
                    <td>JCI{{date('dmy', strtotime($order->created_at))}}-{{str_pad($order->customer->id, 4, '0', STR_PAD_LEFT)}}</td>
                    <td>
                        <p>Name : {{$order->customer->name}}</p>
                        <p>Phone : {{$order->customer->phone_number}}</p>
                        <p>Address : {{$order->customer->address}}</p>
                    </td>
                    <td style="  white-space: nowrap;">
                        @foreach($order->orderproducts as $orderproduct)
                        <p>{{$orderproduct->product->name}} x {{$orderproduct->quantity}} unit</p>
                        @endforeach
                    </td>
                    <td>RM {{$order->total}}</td>
                    <td>{{$order->order_status}}</td>
                    <td>@if ($order->order_status == 'Pending on Stokis') <button type="submit" class="btn gray-background ">Edit</button> @endif</td>
                </tr>
            </form>
            @endforeach
            </tbody>
        </table>
        <div class="text-xs-center mt-4">
            {{ $orders->links() }}
        </div>
    </div>
</div>

@endsection




