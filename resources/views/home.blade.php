@extends('layouts.app')
@if(Auth::user()->role == 'stokis' || Auth::user()->role == 'agent')
    <script>window.location = "/dashboard";</script>
@elseif(Auth::user()->role == 'admin')
    <script>window.location = "/admin";</script>
@else

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@endif
