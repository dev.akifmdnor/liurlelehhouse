
@extends('layouts.layout')

@section('description')
<div>
    <h4>Order Overview</h4>
    <p> Orders are listed here.</p>
</div>
@endsection

@section('content')
@if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
@endif
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br/>
@endif

<div class="container-fluid dashboard">
    <div class="row">
        <div class="col-md-12 white-background purchase">
            <div class="row">
                <div class="col-md-12 header-purchase">
                    @if (Route::currentRouteName() == 'admin.pending')
                    <h5>  Pending Order </h5>
                    @else
                    <h5>  Order History</h5>
                    @endif
                </div>
            </div>
            <table class="table-bordered order-table">
                <thead>
                    <tr>
                        <th>Batch Number</th>
                        <th>Order Number</th>
                        <th>Stokis ID</th>
                        <th>Stokis Details</th>
                        <th>Order Details</th>
                        <th>Total Price</th>
                        <th>Last Updated</th>
                        @if (Route::currentRouteName() == 'admin.pending')
                        <th>Action</th>
                        @else
                        <th>Status</th>
                        @endif
                    </tr>
                </thead>
                <tbody>

                @foreach($order_hqs as $order_hq)
                <tr>
                    <td>JBN{{date('dmy', strtotime($order_hq->created_at))}}-{{str_pad($order_hq->id, 4, '0', STR_PAD_LEFT)}}</td>
                    <td>
                        @foreach($order_hq->orders as $order)
                        <p>JON{{date('dmy', strtotime($order->created_at))}}-{{str_pad($order->id, 4, '0', STR_PAD_LEFT)}}</p>
                        @endforeach
                    </td>
                    <td>JSI{{date('dmy', strtotime($order_hq->user->created_at))}}-{{str_pad($order_hq->user->id, 4, '0', STR_PAD_LEFT)}}</td>
                    <td>
                        <p>Name : {{$order_hq->user->name}}</p>
                        <p>Phone : {{$order_hq->user->phone_number}}</p>
                        <p>Address : {{$order_hq->user->address}}</p>
                    </td>
                    <td style="  white-space: nowrap;">
                        @foreach($products as $product)
                        @php
                            $quantity=0;
                            foreach ($order_hq->orderproducts as $orderproduct) {
                               if($orderproduct->product->id == $product->id)
                               $quantity = $quantity + $orderproduct->quantity;
                            }
                        @endphp
                        <p>{{$product->name}} x {{$quantity}} unit</p>
                        @endforeach
                    </td>
                    <td>RM {{$order_hq->total}}</td>
                    <td>{{$order_hq->updated_at}}</td>
                    @if (Route::currentRouteName() == 'admin.pending')
                    <td>
                        <form method="get" action="{{ route('admin.edit', $order_hq->id) }}"  enctype="multipart/form-data">
                            @csrf
                            <button type="submit" class="btn gray-background ">Approve</button>
                        </form>
                        <form method="get" action="{{ route('admin.reject', $order_hq->id) }}"  enctype="multipart/form-data">
                            @csrf
                            <button type="submit" class="btn gray-background ">Reject</button>
                        </form>
                    </td>
                    @else
                    <td>{{$order_hq->status}} </td>
                    @endif
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="text-xs-center">
        {{ $order_hqs->links() }}
    </div>
</div>
@endsection
