
@extends('layouts.layout')

@section('description')
<div>
    <h4>Order Overview</h4>
    <p> Orders are listed here.</p>
</div>
@endsection

@section('content')
@if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
@endif
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br/>
@endif

{{--@foreach($order_hqs->groupBy('approved_at') as $day_order=>$order)
<pre>
    {{var_dump($order[0]->attributes,true)}}
</pre>
@endforeach--}}

<div class="container-fluid dashboard">
    <div class="row">
        <div class="col-md-12 white-background purchase">
            <div class="row">
                <div class="col-md-12 header-purchase">
                    <h5> Order Summary by day (Approved by HQ)</h5>
                </div>
            </div>
            <table class="table-bordered order-table">
                <thead>
                    <tr>
                        <th>Approved Date</th>
                        <th>Batch Number</th>
                        <th>Order Details</th>
                        <th>Total Sale Per Day</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach($order_hqs->groupBy('approved_at') as $day_order=> $orders)
                        <tr>
                        <td>{{$orders[0]->approved_at}}</td>
                        <td>
                        @foreach($orders as $order)
                           <p> JBN{{date('dmy', strtotime($order->created_at))}}-{{str_pad($order->id, 4, '0', STR_PAD_LEFT)}} </p>
                        @endforeach
                        </td>

                        <td>
                        @foreach($products as $product)
                            @php

                                $quantity=0;
                                foreach($orders as $order) {
                                    foreach ($order_hqs as $order_hq) {
                                        if($order_hq->id == $order->id){
                                            foreach ($order_hq->orderproducts as $orderproduct) {
                                                if($orderproduct->product->id == $product->id)
                                                    $quantity = $quantity + $orderproduct->quantity;
                                            }
                                        }
                                    }
                                }
                            @endphp
                        <p>{{$product->name}} x {{$quantity}} unit</p>
                        @endforeach
                        </td>
                        <td>
                        @php
                            $totalprice = 0.00;

                        foreach($orders as $order)
                            $totalprice  =  $totalprice + $order->total;
                        @endphp
                        RM {{$totalprice}}
                        </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
