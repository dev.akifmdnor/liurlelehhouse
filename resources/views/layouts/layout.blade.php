<!doctype html>
<html lang="en">
  <head>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/sidebar.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/common.css') }}">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  @yield('styles')
  <title>Liur Leleh House</title>
  </head>
  <body>
    <div class="container-fluid full-width ">
        <div class="row">
            <div class="col-md-2 full-width">
                <div class="nav-side-menu">
                    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
                    <div class="brand">
                        <img class="logo mx-auto d-block" src="{{ URL::asset('images/logo.png') }}"/>
                    </div>
                    <div class="menu-list">
                        <ul id="menu-content" class="menu-content collapse out">

                            @if(Auth::user()->role == 'stokis' || Auth::user()->role == 'agent' )
                            <li class="{{ Route::currentRouteName() == 'orders.create' ? 'active' : '' }}">
                                <a href="{{ route('orders.create')}}">
                                    <h1><i class="fas fa-shopping-cart"></i></h1>
                                    <h5>PRODUCT PURCHASE</h5>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'dashboard.index' ? 'active' : '' }}">
                                <a href="{{ route('dashboard.index')}}">
                                    <h1><i class="fas fa-laptop"></i></h1>
                                    <h5>DASHBOARD OVERVIEW</h5>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'profile.index' ? 'active' : '' }}">
                                <a href="{{ route('profile.index')}}">
                                    <h1><i class="fas fa-user"></i></h1>
                                    <h5>PERSONAL PROFILE</h5>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'customer' ? 'active' : '' }}">
                                <a href="{{ route('customer')}}">
                                    <h1><i class="fas fa-users"></i></h1>
                                    <h5>CUSTOMER DATABASE</h5>
                                </a>
                            </li>
                            @endif
                            @if(Auth::user()->role == 'stokis')
                            <li class="{{ Route::currentRouteName() == 'orders.approve' ? 'active' : '' }}">
                                <a href="{{ route('orders.approve')}}">
                                    <h1><i class="fas fa-clipboard-check"></i></h1>
                                    <h5>APPROVE ORDERS</h5>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'orders.history' ? 'active' : '' }}">
                                    <a href="{{ route('orders.history')}}">
                                        <h1><i class="fas fa-business-time"></i></h1>
                                        <h5>ORDER HISTORY</h5>
                                    </a>
                                </li>
                            @endif
                            @if(Auth::user()->role == 'admin')
                            <li class="{{ Route::currentRouteName() == 'admin.pending' ? 'active' : '' }}">
                                <a href="{{ route('admin.pending')}}">
                                    <h1><i class="fas fa-briefcase"></i></h1>
                                    <h5>PENDING ORDERS</h5>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'admin.index' ? 'active' : '' }}">
                                <a href="{{ route('admin.index')}}">
                                    <h1><i class="fas fa-business-time"></i></h1>
                                    <h5>ORDERS HISTORY</h5>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'admin.summary' ? 'active' : '' }}">
                                    <a href="{{ route('admin.summary')}}">
                                        <h1><i class="fas fa-clipboard-list"></i></h1>
                                        <h5>ORDER SUMMARY</h5>
                                    </a>
                                </li>
                            <li class="{{ Route::currentRouteName() == 'admin.customer.index' ? 'active' : '' }}">
                                <a href="{{ route('admin.customer.index')}}">
                                    <h1><i class="fas fa-users"></i></h1>
                                    <h5>CUSTOMER DATABASE</h5>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'admin.stokis.index' ? 'active' : '' }}">
                                <a href="{{ route('admin.stokis.index')}}">
                                    <h1><i class="fas fa-user"></i></h1>
                                    <h5>STOKIS / AGENT DATABASE</h5>
                                </a>
                            </li>
                            <li class="{{ Route::currentRouteName() == 'register' ? 'active' : '' }}">
                                <a href="{{ route('register')}}">
                                    <h1><i class="fas fa-user-plus"></i></h1>
                                    <h5>ADD STOKIS / AGENT</h5>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-10 full-width">
                <div class="container-fluid" id="main">
                    <div class="row">
                        <div class="navbar pb-0">
                            <div class="col-md-10 pl-3 mt-3 ">
                                @yield('description')
                            </div>
                            <div class="col-md-2 pr-3">
                                <p class="nametag text-right mb-3 pr-2 black-font">
                                   <b> Welcome , {{Auth::user()->name}} ({{Auth::user()->role}})</b>
                                </p>
                                <div class="notification pr-2 ">
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('LOG OUT   ') }}
                                             <i class="fas fa-sign-out-alt yellow"></i>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12  full-width">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /#sidebar-wrapper -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ URL::asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ URL::asset('js/popper.min.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    </script>
    @yield('scripts')
  </body>
</html>
