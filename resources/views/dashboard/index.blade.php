@auth
@extends('layouts.layout')

@section('description')
<div>
    <h4>Dashboard Overview</h4>
    <p> Welcome to Jerux Liur Leleh Dashboard</p>
</div>
@endsection

@section('content')
<div class="container-fluid dashboard">
    <div class="row">
        <div class="col-md-12 white-background profile">
            <div class="row">
                <div class="col-md-4">
                    <div id="circle" style=""> </div>
                </div>
                <div class="col-md-8">
                   <p class="profile-name">{{$user->name}}</p>
                   <br>
                   <hr>
                   <p>Umur : {{$user->age}}</p>
                   <p>Location : {{$user->address}}</p>
                   <p>Register Date : {{date('d-m-Y', strtotime($user->created_at))}}</p>
                   <hr>
                   <p>{{$user->description}}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md white-background summary text-center mx-3 mb-4">
            <h4>Total</h4>
            <h4>Customer</h4>
            <hr>
            <div class="counter">
                <p class="yellow very-large-font">{{count($user->customers)}}</p>
                <p> Customer</p>
            </div>
        </div>
        <div class="col-md white-background summary mx-3 mb-4">
            <h4>Product</h4>
            <h4>Purchase</h4>
            <hr>
            <div class="counter">
            <p class="yellow very-large-font">{{ $totalunit}}</p>
                <p> Unit</p>
            </div>
        </div>
        <div class="col-md white-background summary mx-3 mb-4">
            <h4>Amount</h4>
            <h4>Purchase</h4>
            <hr>
            <div class="counter">
                <p>RM</p>
                <p class="yellow very-large-font">{{$totalpurchase}}</p>
            </div>
        </div>
    </div>
</div>
@endsection



@section('styles')
    @if(!empty($user->picture[0]->file_name))
    <style>
            #circle
            {
                background:url({{Storage::url($user->picture[0]->file_name)}});
                background-size: cover;
                border-radius:50% 50% 50% 50%;
                width:200px;
                height:200px;
                margin: 0 auto;
            }
    </style>
    @else
    <style>
        #circle
        {
            background:url({{Storage::url('blank.png')}});
            background-size: cover;
            border-radius:50% 50% 50% 50%;
            width:200px;
            height:200px;
            margin: 0 auto;
        }
    </style>
    @endif
@endsection

@else
    <script>window.location = "/";</script>
@endauth
