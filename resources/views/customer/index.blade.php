@auth
@extends('layouts.layout')

@section('description')
<div>
    <h4>Customer Database</h4>
    <p> Here you can view all Customer Database.</p>
</div>
@endsection

@section('content')
@if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
@endif
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br/>
@endif
<div class="container-fluid dashboard">
    <div class="row">
        <div class="col-md-12 white-background purchase">
            <div class="row">
                <div class="col-md-12 header-purchase">
                    <h5> Customer Database</h5>
                </div>
            </div>
            <table class="table-bordered order-table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Date Registered</th>
                        <th>Status & ID</th>
                        <th>Stokis Details</th>
                        <th>Order History</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{date('d/m/Y', strtotime($user->created_at))}}</td>
                    <td>JCI{{date('dmy', strtotime($user->created_at))}}-{{str_pad($user->id, 4, '0', STR_PAD_LEFT)}}</td>
                    <td>
                        <p>Name : {{$user->name}}</p>
                        <p>Phone : {{$user->phone_number}}</p>
                        <p>Address : {{$user->address}}</p>
                    </td>
                    <td>
                    @foreach($new_user->user as $current_user)
                        @if($current_user->id == $user->id)
                            @foreach($current_user->product as $product)
                            <p>{{$product->name}} x {{$product->quantity}} unit</p>
                            @endforeach
                        @endif
                    @endforeach
                    </td>
                    </p></td>
                    <td>
                        <p><a class="dropdown-item" href="{{ route('customer.edit',$user->id)}}">Edit</a></p>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@else
    <script>window.location = "/";</script>
@endauth
