@auth
@extends('layouts.layout')

@section('description')
<div>
    <h4>Dashboard Overview</h4>
    <p> Welcome to Jerux Liur Leleh Product Purchase Dashboard.</p>
</div>
@endsection

@section('content')
@if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
@endif
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div><br/>
@endif
<div class="container-fluid dashboard">
    <div class="form-style">
        <div class="row">
            <div class="col-md-12 white-background purchase">
                <div class="row">
                    <div class="col-md-12 header-purchase">
                        <h5> Edit Customer</h5>
                    </div>
                </div>
                <form method="post" action="{{ route('customerorders.update', $customer->id) }}"  enctype="multipart/form-data">
                    @method('PATCH')
                    @csrf
                    <div class="form-style">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="name" class="gray-hint">Nama</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input type="text" class="form-control textbox-style" name="name" value="{{$customer->name}}" />
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="phone_number" class="gray-hint">No. Phone</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <input type="text" class="form-control textbox-style" name="phone_number"  value="{{$customer->phone_number}}" />
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="address" class="gray-hint">Alamat Rumah</label>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group">
                                    <textarea class="form-control" rows="3" cols="60" name="address">{{$customer->address}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-3 offset-md-9">
                                <div class="form-group button-holder">
                                    <button type="submit" class="btn btn-primary yellow-btn"> SAVE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


@endauth
