<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*
    Customer Route
*/
Route::resource('customerorder', 'CustomerOrderController');


/*
    Stokis Route
*/
Route::get('admin/stokis/dashboard/{id}', 'StokisController@dashboard')->name('admin.stokis.dashboard');
Route::get('admin/stokis/order/{id}', 'StokisController@order')->name('admin.stokis.order');
Route::resource('admin/stokis', 'StokisController', ['as' => 'admin']);

/*
    Customer
*/
Route::resource('admin/customer', 'CustomerController', ['as' => 'admin']);
Route::get('customers', 'CustomerController@getAll')->name('customer');
Route::get('customer/edit{id}', 'CustomerController@edit')->name('customer.edit');


/*
    Order Route
*/
Route::get('orders/stokisapprove/{id}', 'OrderController@stokisapprove')->name('orders.stokisapprove');
Route::get('orders/stokisreject/{id}', 'OrderController@stokisreject')->name('orders.stokisreject');
Route::get('orders/approve', 'OrderController@approve')->name('orders.approve');
Route::get('orders/history', 'OrderController@history')->name('orders.history');
Route::resource('orders', 'OrderController');

/*
    Personal Profile Route
*/
Route::resource('profile', 'ProfileController');

/*
    Personal Profile Route
*/
Route::resource('dashboard', 'DashboardController');

/*
    Customer Route
*/
Route::get('customer/get/{id}', 'CustomerOrderController@getCustomer');
Route::resource('customerorders', 'CustomerOrderController');





/*
    Admin Route
*/
Route::get('admin/summary', 'AdminOrderController@summary')->name('admin.summary');
Route::get('admin/reject/{id}', 'AdminOrderController@reject')->name('admin.reject');
Route::get('admin/pending', 'AdminOrderController@pending')->name('admin.pending');
Route::resource('admin', 'AdminOrderController');

