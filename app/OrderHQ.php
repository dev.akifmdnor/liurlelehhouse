<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderHQ extends Model
{
    protected $table = 'order_hqs';

    protected $fillable = [
        'total',
        'status',
        'approve_at',
        'user_id',
    ];

    public function orders()
    {
        return $this->hasMany('App\Order','orderhq_id','id');
    }

    public function orderproducts()
    {
        return $this->hasMany('App\OrderProduct','orderhq_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
