<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Picture extends Model
{
    use SoftDeletes;
    protected $fillable = ['model_id', 'model_name' , 'file_name'];

    public function user()
    {
        return $this->belongsTo('App\Branch', 'model_id');
    }
    
}
