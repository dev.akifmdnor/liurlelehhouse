<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;

class CustomerOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::id());
        return view('orders.customer', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::find(Auth::id());
        return view('orders.customer', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'address'=> 'required',
            'phone_number' => 'required',
        ]);

        $customer = new Customer([
            'name' => $request->get('name'),
            'address'=> $request->get('address'),
            'phone_number'=> $request->get('phone_number'),
            'user_id'=> Auth::id(),
        ]);
        $customer->save();
        return redirect('/orders/create')->with('success', 'Customer has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'address'=> 'required',
            'phone_number' => 'required',
        ]);

        $customer = Customer::find($id);
        $customer->name = $request->get('name');
        $customer->phone_number = $request->get('phone_number');
        $customer->address = $request->get('address');
        $customer->save();

        if(Auth::user()->role == 'admin')
            return redirect('/admin/customer')->with('success', 'Customer has been updated.');

        else
            return redirect('/customers')->with('success', 'Customer has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function Customer($id)
    {
        $customer = Customer::find(($id));
        return $customer;
    }
}
