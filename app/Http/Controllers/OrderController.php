<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Customer;
use App\Picture;
use App\Product;
use App\Order;
use App\OrderProduct;
use App\OrderHQ;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::id());
        $customers = Customer::get();
        $products = Product::get();
        $orders = Order::where('user_id',$user->id)->orderBy('updated_at', 'desc')->paginate(config('app.pagination'));
        return view('orders.create', compact('user','users','products','orders'));
    }

    public function approve()
    {
        $orders = Order::where('order_status','Pending on Stokis')
        ->where('stokis_id',Auth::id())
        ->orderBy('updated_at', 'desc')->paginate(config('app.pagination'));
        return view('orders.index', compact('orders'));
    }

    public function history()
    {
        $orders = Order::where('order_status','!=','Pending on Stokis')
        ->where('stokis_id',Auth::id())
        ->orderBy('updated_at', 'desc')->paginate(config('app.pagination'));
        return view('orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::find(Auth::id());
        $customers = Customer::Get();
        $products = Product::Get();
        $orders = Order::where('user_id',$user->id)
        ->orderBy('updated_at', 'desc')->paginate(config('app.pagination'));
       return view('orders.create', compact('user','users','products','orders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'customer_id'=>'required',
        ]);

        $totalOrder = 0.00;
        foreach ($request->get('product_id') as $key => $val)
        {
            $orderLinePrice = $request->input('quantity.'.$key) * $request->input('price.'.$key);
            $totalOrder = $totalOrder + $orderLinePrice;
        }

        if(Auth::user()->leader_id != 0)
            $stokis_id = Auth::user()->leader_id;
        else
            $stokis_id = Auth::id();

        if(Auth::user()->leader_id == 0 && Auth::user()->role =='agent')
            return redirect('/orders/create')->withErrors(['Stokis / Leader is not set. Please contact HQ']);

        $order = new Order([
            'user_id' => Auth::id(),
            'customer_id'=> $request->get('customer_id'),
            'order_status'=> 'Pending on Stokis',
            'total'=> $totalOrder,
            'stokis_id' => $stokis_id,
        ]);
        $order->save();

        foreach ($request->get('product_id') as $key => $val)
        {
            $orderproduct = new OrderProduct([
                'order_id' => $order->id,
                'product_id' => $request->input('product_id.'.$key),
                'quantity' => $request->input('quantity.'.$key),
            ]);
            $orderproduct->save();
        }
        return redirect('/orders/create')->with('success', 'Order has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $order = Order::find($id);
        $user = $order->user;
        $products = Product::Get();
        return view('orders.edit', compact('order','user','products'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'customer_id'=>'required',
        ]);

        $order = Order::find($id);
        $order->customer = $request->get('customer_id');

        $totalOrder = 0.00;
        foreach ($request->get('orderproduct_id') as $key => $val)
        {
            $orderLinePrice = $request->input('quantity.'.$key) * $request->input('price.'.$key);
            $totalOrder = $totalOrder +$orderLinePrice;
        }

        $order->total = $totalOrder;
        $order->save();

        foreach ($request->get('orderproduct_id') as $key => $val)
        {

            $orderproduct = OrderProduct::find($request->input('orderproduct_id.'.$key));
            $orderproduct->quantity = $request->input('quantity.'.$key);
            $orderproduct->save();
        }
        return redirect('/orders/create')->with('success', 'Orders has been updated');
    }

    public function stokisapprove($id)
    {

        $totalOrder = 0;
        $orderhq = new OrderHQ([
            'status'=> 'Pending on HQ',
            'user_id'=> $id,
            'total'=> $totalOrder,
        ]);

        $orders= Order::where('stokis_id' , $id)->where('order_status', 'Pending on Stokis')->get();
        $orderhq->save();

        foreach ($orders as $order_array) {
            $order = Order::find($order_array->id);
            $order->order_status = 'Pending on HQ';
            $order->save();


            $order->orderhq_id =  $orderhq->id;
            $order->save();

            $orderproducts = OrderProduct::where('order_id',$order_array->id)->get();
            foreach ($orderproducts as $orderproduct) {
                $orderproduct = OrderProduct::find($orderproduct->id);
                $orderproduct->orderhq_id = $orderhq->id;
                $orderproduct->save();
            }

            $totalOrder = $totalOrder +  $order->total;

        }

        $orderhq->total = $totalOrder;
        $orderhq->save();

        return redirect('/orders/approve')->with('success', 'Order has been approved and sent to HQ.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function stokisreject($id)
    {
        $order = Order::find($id);
        $order->order_status = 'Rejected by Stokis';
        $order->save();
        return redirect('/orders/approve')->with('success', 'Order has been rejected.');
    }
}
