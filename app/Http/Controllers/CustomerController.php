<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;
use App\Product;
use App\User;

use Illuminate\Support\Facades\Auth;


class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::user()->role == 'admin')
            $users = Customer::paginate(config('app.pagination'));

        else{
            $user = User::find(Auth::id());
            $user = $user->customers;
        }

        $products = Product::get();

        // create new user object and calculate total product and quantity per user
        $new_user = new \stdClass();
        $userCount= 0;
        foreach($users as $user){
            $productCount = 0;
            $new_user->user[$userCount] = new \stdClass();
            $new_user->user[$userCount]->id = $user->id;

            foreach($products as $product){
                $new_user->user[$userCount]->product[$productCount]  = new \stdClass();
                $new_user->user[$userCount]->product[$productCount]->id = $product->id;
                $new_user->user[$userCount]->product[$productCount]->name = $product->name;
                $new_user->user[$userCount]->product[$productCount]->quantity = 0;
                foreach($user->orders as $order){
                    if ($order->order_status == 'Approved by HQ')
                        foreach($order->orderproducts as $orderproduct){
                            if ($orderproduct->product_id == $product->id)
                            $new_user->user[$userCount]->product[$productCount]->quantity = $new_user->user[$userCount]->product[$productCount]->quantity + $orderproduct->quantity;
                        }
                }

            $productCount++;
            }
        $userCount++;
        }
        return view('admin.customer', compact('users','products', 'new_user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('customer.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // get all for stokis

    public function getAll()
    {

        $users = Customer::where('user_id',Auth::id())->paginate(config('app.pagination'));

        $products = Product::get();

        // create new user object and calculate total product and quantity per user
        $new_user = new \stdClass();
        $userCount= 0;
        foreach($users as $user){
            $productCount = 0;
            $new_user->user[$userCount] = new \stdClass();
            $new_user->user[$userCount]->id = $user->user_id;

            foreach($products as $product){
                $new_user->user[$userCount]->product[$productCount]  = new \stdClass();
                $new_user->user[$userCount]->product[$productCount]->id = $product->id;
                $new_user->user[$userCount]->product[$productCount]->name = $product->name;
                $new_user->user[$userCount]->product[$productCount]->quantity = 0;
                foreach($user->orders as $order){
                    if ($order->order_status == 'Approved by HQ')
                    foreach($order->orderproducts as $orderproduct){
                        if ($orderproduct->product_id == $product->id)
                        $new_user->user[$userCount]->product[$productCount]->quantity = $new_user->user[$userCount]->product[$productCount]->quantity + $orderproduct->quantity;
                    }
                }

            $productCount++;
            }
        $userCount++;
        }
        return view('customer.index', compact('users','products', 'new_user'));
    }

}
