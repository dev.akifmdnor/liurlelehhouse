<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Picture;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::id());
        $users = User::get();
        return view('profile.index', compact('user','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'name'=>'required',
        ]);

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->age = $request->get('age');
        $user->phone_number = $request->get('phone_number');
        $user->address = $request->get('address');
        $user->description = $request->get('description');
        $user->bank_name = $request->get('bank_name');
        $user->bank_number = $request->get('bank_number');
        $user->leader_id = $request->get('leader_id');
        $user->save();

        if($request->hasFile('pictures')){
            $allowedfileExtension=['jpg','png'];
            $files = $request->file('pictures');
            foreach ($files as $file) {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check=in_array($extension,$allowedfileExtension);
                //dd($check);

                if($check)
                {
                    foreach ($request->pictures as $picture) {
                        $filename = $picture->store('public');

                        if (!empty($user->picture[0]->id))
                        {
                            $picture = Picture::find($user->picture[0]->id);
                            $picture->file_name = $filename;
                            $picture->save();
                        }
                        else
                        {
                            Picture::create([
                                'model_id' => $user->id,
                                'model_name' => 'user',
                                'file_name' => $filename
                            ]);
                        }
                    }

                    if(Auth::user()->role == 'admin')
                        return redirect('/admin/stokis')->with('success', 'Profile has been updated with pictures sucessfully.');

                    else
                        return redirect('/profile')->with('success', 'Profile has been updated with pictures sucessfully.');
                }

                else {
                    if(Auth::user()->role == 'admin')
                        return redirect('/admin/stokis')->with('success', 'Profile as been updated but picture upload failed.');

                    else
                        return redirect('/profile')->with('success', 'Profile as been updated but picture upload failed.');
                }
            }
        }
        if(Auth::user()->role == 'admin')
            return redirect('/admin/stokis')->with('success', 'Profile has been updated.');

        else
            return redirect('/profile')->with('success', 'Profile has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
