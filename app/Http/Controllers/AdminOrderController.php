<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderHQ;
use App\Product;


class AdminOrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get();
        $order_hqs = OrderHQ::where('status','Rejected by HQ')->orWhere('status','Approved by HQ')->orderBy('updated_at', 'desc')
        ->paginate(config('app.pagination'));
        return view('admin.order', compact('order_hqs', 'products'));
    }

    public function pending()
    {
        $products = Product::get();
        $order_hqs = OrderHQ::where('status','Pending on HQ')->orderBy('updated_at', 'desc')
        ->paginate(config('app.pagination'));

        return view('admin.order', compact('order_hqs','products'));
    }

    public function summary()
    {
        $products = Product::get();
        $order_hqs = OrderHQ::where('status','Approved by HQ')->orderBy('updated_at', 'desc')
        ->get();

        return view('admin.summary', compact('order_hqs','products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order_hq = OrderHQ::find($id);
        $order_hq->status = 'Approved by HQ';
        $order_hq->approved_at = now();
        $order_hq->save();

        $orders= Order::where('orderhq_id' , $id)->get();

        foreach ($orders as $order_array) {
            $order = Order::find($order_array->id);
            $order->order_status = 'Approved by HQ';
            $order->save();
        }

        return redirect('/admin/pending')->with('success', 'Batch Order has been approved.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reject($id)
    {
        $order_hq = OrderHQ::find($id);
        $order_hq->status = 'Rejected by HQ';
        $order_hq->save();

        $orders= Order::where('orderhq_id' , $id)->get();

        foreach ($orders as $order_array) {
            $order = Order::find($order_array->id);
            $order->order_status = 'Rejected by HQ';
            $order->save();
        }

        return redirect('/admin/pending')->with('success', 'Batch Order has been rejected.');
    }
}
