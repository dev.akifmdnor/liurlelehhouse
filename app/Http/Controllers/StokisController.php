<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Product;
use App\Order;
use App\Customer;

class StokisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get();
        $users = User::where('role','stokis')->orWhere('role','agent')->paginate(config('app.pagination'));

        // create new user object and calculate total product and quantity per user
        $new_user = new \stdClass();
        $userCount= 0;
        foreach($users as $user){
            $productCount = 0;
            $new_user->user[$userCount] = new \stdClass();
            $new_user->user[$userCount]->id = $user->id;

            foreach($products as $product){
                $new_user->user[$userCount]->product[$productCount]  = new \stdClass();
                $new_user->user[$userCount]->product[$productCount]->id = $product->id;
                $new_user->user[$userCount]->product[$productCount]->name = $product->name;
                $new_user->user[$userCount]->product[$productCount]->quantity = 0;
                foreach($user->orders as $order){
                    if ($order->order_status == 'Approved by HQ')
                    foreach($order->orderproducts as $orderproduct){
                        if ($orderproduct->product_id == $product->id)
                        $new_user->user[$userCount]->product[$productCount]->quantity = $new_user->user[$userCount]->product[$productCount]->quantity + $orderproduct->quantity;
                    }
                }

            $productCount++;
            }
        $userCount++;
        }

        return view('admin.stokis', compact('users','products', 'new_user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::where('role','stokis')->get();
        $user = User::find($id);
        return view('profile.index', compact('user','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dashboard($id)
    {
        $user = User::find($id);
        $orders =  Order::where('order_status', 'Approved by HQ')->where('user_id', $user->id)->get();;
        $totalpurchase = Order::where('order_status', 'Approved by HQ')->where('user_id', $user->id)->sum('total');
        $totalunit = 0;
        foreach($orders as $order)
            $totalunit = $totalunit + $order->orderproducts->sum('quantity');

        return view('dashboard.index', compact('user','totalpurchase','orders','totalunit'));
    }

    public function order($id)
    {
        $user = User::find($id);
        $customers = Customer::Get();
        $products = Product::Get();
        $orders = Order::where('user_id',$user->id)->orderBy('updated_at', 'desc')->paginate(config('app.pagination'));
        return view('orders.create', compact('user','users','products','orders'));
    }
}
