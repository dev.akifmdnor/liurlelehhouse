<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'remember_token',
        'age',
        'phone_number',
        'address',
        'description',
        'bank_name',
        'bank_number',
        'role',
        'leader_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function picture()
    {
        return $this->hasMany('App\Picture' , 'model_id')->where('model_name', '=', 'user');
    }

    public function customers()
    {
        return $this->hasOne('App\Customer');
    }

    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    function leader() {
        return $this->belongsTo('App\User', 'leader_id', 'id');
    }
}

